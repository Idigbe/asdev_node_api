
const express = require('express');
const bodyParser = require('body-parser');
// var cors = require('cors');
const app = express(); var port=process.env.port||8080;

// var corsOptions = {
//   origin: 'http://localhost:4200',
//   optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
// }
// const admin = require('firebase-admin');

// const serviceAccount = require('./serviceAccountKey.json');

// admin.initializeApp({
//   credential: admin.credential.cert(serviceAccount)
// });

// const db = admin.firestore();app.locals.db=db;
// Setup
// var app = express(); var port=process.env.port;

//CORS middleware


// const authRoute = require('./apis/auth'); //authRoutes(app); 
// app.set('trust proxy', true);
// Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// 
// app.use('/auth',authRoute);
var allowCrossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  // res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', "Origin, X-Requested-With, Content-Type, Accept, x-client-key, x-client-token, x-client-secret, Authorization,xmobile");
  next();

}
app.use(allowCrossDomain);
// app.use(cors())



// var userRoutes = require('./apis/users'); userRoutes(app); 
// var paymentsRoutes = require('./apis/payments'); paymentsRoutes(app); 
// var docsRoutes = require('./apis/docs'); docsRoutes(app); 
// var seedRoutes = require('./apis/seeds'); seedRoutes(app); 

// app.use(function(req,res,next){
//   res.header('Access-Control-Allow-Origin','*'),
//   res.header('Access-Control-Allow-Methods','GET,POST,PUT,DELETE'),
//   res.header('Access-Control-Allow-Headers','Content-Type');
//   next();
// })

app.get('/',function(req,res){res.send('ASDEV API X3');});
app.listen(port); console.log("ASDEV Server running at http://localhost:%d",port);
app.on('listening',function(){  console.log('ok, server is running');});
 