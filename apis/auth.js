'use strict';
const express = require('express');
const router = express.Router();
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var moment = require('moment');
const db = require('./db');
// const db = admin;
var cors = require('cors');

var corsOptions = {
  origin: 'http://localhost:4200',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}
// module.exports = function(app) {
  router.route('/login').post(cors(corsOptions),
        function(req, res) { 
          // res.setHeader('Access-Control-Allow-Origin','*'),
          // res.setHeader('Access-Control-Allow-Methods','GET,HEAD,OPTIONS,PUT,POST,DELETE'),
          // res.setHeader('Access-Control-Allow-Headers','Content-Type');
          // next();
          db.collection('xUsers').where('mobile', '==', req.body.mobile).get().then(function(docs)
         {
           if(docs.size>0)
           {//Get the salt and the search against the pin
            var xDoc=''; 
            docs.forEach(function (documentSnapshot) { xDoc = documentSnapshot.data(); });            
            // console.log('XDoc: '+JSON.stringify(xDoc))
            var hashRes=saltHashPassword(req.body.xpin,xDoc.hasher);
            db.collection('xUsers').where('xpin', '==', hashRes.passwordHash).get().then(function(docs2)
            {
              if(docs2.size>0)
              {
                var xDoc2='';var docId=''; 
                docs2.forEach(function (documentSnapshot2) { xDoc2 = documentSnapshot2.data(); docId = documentSnapshot2.id; }); 
                 var token= jwt.sign({mobile:req.body.mobile}, req.body.mobile,{ expiresIn: '4h' });
                  req.body['createdAt']=Date.now();req.body['xCreatedAt']=moment(Date.now()).format('DD MM YYYY, HH:mm:ss');
                  //Decode token looks like: {"mobile":"+2348098367527","iat":1544724567,"exp":1544738967}
                  var decoded_verify = jwt.verify(token, req.body.mobile); 
                  try {
                  // var decoded = jwt.decode(token); //console.log("Decoded X: "+JSON.stringify(decoded))
                  xDoc2['_id']=docId;
                  xDoc2["xauth"]={token:token,'ua':req.headers['user-agent'],'createdAt':Date.now(),'xCreatedAt':moment(Date.now()).format('DD MM YYYY, HH:mm:ss') }; 
                    //Update the object in the db
                  db.collection('xUsers').doc(docId).update(xDoc2).then(new_doc=>{
                    // console.log('XDoc2 ID: '+JSON.stringify(new_doc._writeTime._seconds))
                    if(new_doc._writeTime._seconds)  {res.send(JSON.stringify(xDoc2)); }
                    else //User Exists
                    { res.send("Could/Did NOT Update the User!!");  }
                });
              
                } catch(decode_err) {console.log("decode_err :"+decode_err)}
                
              }
              else //User Exists
              { res.send("User Password is wrong!!");  }
            });

           }
           else //User Exists
           { res.send("Mobile Does not exist!!");  }
         });
    
    });
    router.route('/auth/activateUser').post(
      function(req, res) { 
       var query={"regCode": req.body.m,"authPin": req.body.a};
       db.collection('xUsers').find(query).toArray(function(findErr,docs)
       {
         if (findErr) throw findErr; 
         if(docs.length>0)
         {//Get the salt and the search against the pin
          docs[0]['isActivated']=true;
          docs[0]['authPin']='';
          var upquery={"_id": docs[0]._id};var new_obj={$set:docs[0]}
          db.collection('xUsers').updateOne(upquery,new_obj,function(upErr,upDocs)
          {
            if (upErr) throw upErr; 
            console.log('upDocs: '+JSON.parse(upDocs).nModified)
            if(JSON.parse(upDocs).nModified>0)
            {//Get the salt and the search against the pin
             res.send('Ok')
            }
            else //User Exists
            { res.send("Could/Did NOT Update the User!!");  }
          });
         }
         else //User Exists
         { res.send("User Does not exist!!");  }
       });
  
  });
  router.route('/auth/toggleActivations').post(
    function(req, res) { 
      var ua=req.headers['user-agent'];
      confirmAuthorization(req.headers['authorization'],ua).then(doc=>{
        ///////////////////////
        if(doc.length>0){
          var query={"_id":  new mongodb.ObjectID(req.body.xid)};
          db.collection('xUsers').find(query).toArray(function(findErr,docs)
          {
            if (findErr) throw findErr; 
            if(docs.length>0)
            {
              docs[0]['isActivated']=req.body.status;
              var upquery={"_id": docs[0]._id};var new_obj={$set:docs[0]}
              db.collection('xUsers').updateOne(upquery,new_obj,function(upErr,upDocs)
              {
                if (upErr) throw upErr; 
                // console.log('upDocs: '+JSON.parse(upDocs).nModified)
                if(JSON.parse(upDocs).nModified>0)
                {
                res.send('Ok')
                }
                else //User Exists
                { res.send("User record to be updated is the same!!");  }
              });
            }
            else //User Exists
            { res.send("User Does not exist!!");  }
          });
        }
        else {res.send('invalid')}
        ///////////////////////
      })
});

function confirmAuthorization(mobile,auth,ua){
  // console.log('Auth mobile: '+mobile);console.log('Auth auth: '+auth);console.log('Auth ua: '+ua)
  return new Promise((resolve,reject)=>{
     db.collection('xUsers').where('mobile', '==', mobile).where('xauth.token', '==', auth).where('xauth.ua', '==', ua).get().then(function(docs)
     {
      // console.log('Auth Doc Size: '+docs.size)
          if(docs.size>0)
          {
            var xDoc=''
            docs.forEach(function (documentSnapshot) { xDoc = documentSnapshot.data();}); 
            // console.log('Auth DocSnapshot: '+documentSnapshot.data())
            if (moment().diff(xDoc.xauth.createdAt, 'days') >=1) { resolve([]) ;  }else  { resolve([xDoc]); }
          }
          else  { resolve([]); }
     });
  })
}
    /**
 * hash password with sha512.
 * @function
 * @param {string} password - List of required fields.
 * @param {string} salt - Data to be validated.
 */
var sha512 = function(password, salt){
    var hash = crypto.createHmac('sha512', salt); /** Hashing algorithm sha512 */
    hash.update(password);
    var value = hash.digest('hex');
    return {
        salt:salt,
        passwordHash:value
    };
  };
  function saltHashPassword(userpassword,salt) {
    var passwordData = sha512(userpassword, salt);
    // console.log('UserPassword = '+userpassword); console.log('Passwordhash = '+passwordData.passwordHash);console.log('nSalt = '+passwordData.salt);
    return passwordData;
  }
//   module.exports.confirmAuthorization = confirmAuthorization;
// };
module.exports = router;