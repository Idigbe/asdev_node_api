'use strict';
var moment = require('moment');
var auth=require('./auth');

module.exports = function(app) {  

app.route('/seeds/getAllChapters').get(
    function(req, res) { 
      var ua=req.headers['user-agent'];
      auth.confirmAuthorization(req.headers['xmobile'],req.headers['authorization'],ua).then(doc=>{
        if(doc.length>0){
          app.locals.db.collection('xChapters').get().then(function(docs)
          {
            if(docs.size>0)
            {
              var data = docs.docs.map(function (documentSnapshot) { return documentSnapshot.data(); });
              res.send(data)
            }
            else //User Exists
            { res.send([]);  }
          });
        }else {res.send('invalid')}
      })
}); 
app.route('/seeds/getAllDocTypes').get(
  function(req, res) { 
    var ua=req.headers['user-agent'];
    auth.confirmAuthorization(req.headers['xmobile'],req.headers['authorization'],ua).then(doc=>{
      if(doc.length>0){
        app.locals.db.collection('xDocTypes').get().then(function(docs)
        {
          if(docs.size>0)
          {
            var data = docs.docs.map(function (documentSnapshot) { return documentSnapshot.data(); });
            res.send(data)
          }
          else //User Exists
          { res.send([]);  }
        });
      }else {res.send('invalid')}
    })
}); 
app.route('/seeds/getAllLgas').get(
  function(req, res) { 
    var ua=req.headers['user-agent'];
    auth.confirmAuthorization(req.headers['xmobile'],req.headers['authorization'],ua).then(doc=>{
      if(doc.length>0){
        app.locals.db.collection('xLgas').get().then(function(docs)
        {
          if(docs.size>0)
          {
            var data = docs.docs.map(function (documentSnapshot) { return documentSnapshot.data(); });
            res.send(data)
          }
          else //User Exists
          { res.send([]);  }
        });
      }else {res.send('invalid')}
    })
});
app.route('/seeds/getAllMaritalStatus').get(
  function(req, res) { 
    var ua=req.headers['user-agent'];
    auth.confirmAuthorization(req.headers['xmobile'],req.headers['authorization'],ua).then(doc=>{
      if(doc.length>0){
        app.locals.db.collection('xMaritalStatus').get().then(function(docs)
        {
          if(docs.size>0)
          {
            var data = docs.docs.map(function (documentSnapshot) { return documentSnapshot.data(); });
            res.send(data)
          }
          else //User Exists
          { res.send([]);  }
        });
      }else {res.send('invalid')}
    })
});
app.route('/seeds/getAllMemberTypes').get(
  function(req, res) { 
    var ua=req.headers['user-agent'];
    auth.confirmAuthorization(req.headers['xmobile'],req.headers['authorization'],ua).then(doc=>{
      if(doc.length>0){
        app.locals.db.collection('xMemberTypes').get().then(function(docs)
        {
          if(docs.size>0)
          {
            var data = docs.docs.map(function (documentSnapshot) { return documentSnapshot.data(); });
            res.send(data)
          }
          else //User Exists
          { res.send([]);  }
        });
      }else {res.send('invalid')}
    })
});
app.route('/seeds/getAllPositionTypes').get(
  function(req, res) { 
    var ua=req.headers['user-agent'];
    auth.confirmAuthorization(req.headers['xmobile'],req.headers['authorization'],ua).then(doc=>{
      if(doc.length>0){
        app.locals.db.collection('xPositionTypes').get().then(function(docs)
        {
          if(docs.size>0)
          {
            var data = docs.docs.map(function (documentSnapshot) { return documentSnapshot.data(); });
            res.send(data)
          }
          else //User Exists
          { res.send([]);  }
        });
      }else {res.send('invalid')}
    })
});
app.route('/seeds/getAllRelationshipTypes').get(
  function(req, res) { 
    var ua=req.headers['user-agent'];
    auth.confirmAuthorization(req.headers['xmobile'],req.headers['authorization'],ua).then(doc=>{
      if(doc.length>0){
        app.locals.db.collection('xRelationshipTypes').get().then(function(docs)
        {
          if(docs.size>0)
          {
            var data = docs.docs.map(function (documentSnapshot) { return documentSnapshot.data(); });
            res.send(data)
          }
          else //User Exists
          { res.send([]);  }
        });
      }else {res.send('invalid')}
    })
});
app.route('/seeds/getAllRoles').get(
  function(req, res) { 
    var ua=req.headers['user-agent'];
    auth.confirmAuthorization(req.headers['xmobile'],req.headers['authorization'],ua).then(doc=>{
      if(doc.length>0){
        app.locals.db.collection('xRoles').get().then(function(docs)
        {
          if(docs.size>0)
          {
            var data = docs.docs.map(function (documentSnapshot) { return documentSnapshot.data(); });
            res.send(data)
          }
          else //User Exists
          { res.send([]);  }
        });
      }else {res.send('invalid')}
    })
});
app.route('/seeds/getAllSex').get(
  function(req, res) { 
    var ua=req.headers['user-agent'];
    auth.confirmAuthorization(req.headers['xmobile'],req.headers['authorization'],ua).then(doc=>{
      if(doc.length>0){
        app.locals.db.collection('xSex').get().then(function(docs)
        {
          if(docs.size>0)
          {
            var data = docs.docs.map(function (documentSnapshot) { return documentSnapshot.data(); });
            res.send(data)
          }
          else //User Exists
          { res.send([]);  }
        });
      }else {res.send('invalid')}
    })
});

app.route('/seeds/getChapterByID').get(
  function(req, res) { 
    var ua=req.headers['user-agent'];
    auth.confirmAuthorization(req.headers['xmobile'],req.headers['authorization'],ua).then(doc=>{
      if(doc.length>0){
        app.locals.db.collection('xChapters').where("name","==",req.query.xx).get().then(function(docs)
        {
          if(docs.size>0)
          {
            var data = docs.docs.map(function (documentSnapshot) { return documentSnapshot.data(); });
            res.send(data)
          }
          else //User Exists
          { res.send([]);  }
        });
      }else {res.send('invalid')}
    })
}); 
app.route('/seeds/getDocTypeByID').get(
  function(req, res) { 
    var ua=req.headers['user-agent'];
    auth.confirmAuthorization(req.headers['xmobile'],req.headers['authorization'],ua).then(doc=>{
      if(doc.length>0){
        app.locals.db.collection('xDocTypes').where("name","==",req.query.xx).get().then(function(docs)
        {
          if(docs.size>0)
          {
            var data = docs.docs.map(function (documentSnapshot) { return documentSnapshot.data(); });
            res.send(data)
          }
          else //User Exists
          { res.send([]);  }
        });
      }else {res.send('invalid')}
    })
}); 

app.route('/seeds/getLgaByID').get(
  function(req, res) { 
    var ua=req.headers['user-agent'];
    auth.confirmAuthorization(req.headers['xmobile'],req.headers['authorization'],ua).then(doc=>{
      if(doc.length>0){
        var query={"_id":  new mongodb.ObjectID(req.query.xx)};
        app.locals.db.collection('xLgas').where("name","==",req.query.xx).get().then(function(docs)
        {
          if(docs.size>0)
          {
            var data = docs.docs.map(function (documentSnapshot) { return documentSnapshot.data(); });
            res.send(data)
          }
          else //User Exists
          { res.send([]);  }
        });
      }else {res.send('invalid')}
    })
  });
 
app.route('/seeds/getMaritalStatusByID').get(
function(req, res) { 
  var ua=req.headers['user-agent'];
  auth.confirmAuthorization(req.headers['xmobile'],req.headers['authorization'],ua).then(doc=>{
    if(doc.length>0){
      app.locals.db.collection('xMaritalStatus').where("name","==",req.query.xx).get().then(function(docs)
      {
        if(docs.size>0)
        {
          var data = docs.docs.map(function (documentSnapshot) { return documentSnapshot.data(); });
          res.send(data)
        }
        else //User Exists
        { res.send([]);  }
      });
    }else {res.send('invalid')}
  })
});
app.route('/seeds/getMemberTypeByID').get(
function(req, res) { 
  var ua=req.headers['user-agent'];
  auth.confirmAuthorization(req.headers['xmobile'],req.headers['authorization'],ua).then(doc=>{
    if(doc.length>0){
      app.locals.db.collection('xMemberTypes').where("name","==",req.query.xx).get().then(function(docs)
      {
        if(docs.size>0)
        {
          var data = docs.docs.map(function (documentSnapshot) { return documentSnapshot.data(); });
          res.send(data)
        }
        else //User Exists
        { res.send([]);  }
      });
    }else {res.send('invalid')}
  })
});
app.route('/seeds/getPositionTypeByID').get(
function(req, res) { 
  var ua=req.headers['user-agent'];
  auth.confirmAuthorization(req.headers['xmobile'],req.headers['authorization'],ua).then(doc=>{
    if(doc.length>0){
      app.locals.db.collection('xPositionTypes').where("name","==",req.query.xx).get().then(function(docs)
      {
        if(docs.size>0)
        {
          var data = docs.docs.map(function (documentSnapshot) { return documentSnapshot.data(); });
          res.send(data)
        }
        else //User Exists
        { res.send([]);  }
      });
    }else {res.send('invalid')}
  })
});
app.route('/seeds/getRelationshipTypeByID').get(
function(req, res) { 
  var ua=req.headers['user-agent'];
  auth.confirmAuthorization(req.headers['xmobile'],req.headers['authorization'],ua).then(doc=>{
    if(doc.length>0){
      app.locals.db.collection('xRelationshipTypes').where("name","==",req.query.xx).get().then(function(docs)
      {
        if(docs.size>0)
        {
          var data = docs.docs.map(function (documentSnapshot) { return documentSnapshot.data(); });
          res.send(data)
        }
        else //User Exists
        { res.send([]);  }
      });
    }else {res.send('invalid')}
  })
});
app.route('/seeds/getRoleByID').get(
function(req, res) { 
  var ua=req.headers['user-agent'];
  auth.confirmAuthorization(req.headers['xmobile'],req.headers['authorization'],ua).then(doc=>{
    if(doc.length>0){
      app.locals.db.collection('xRoles').where("name","==",req.query.xx).get().then(function(docs)
      {
        if(docs.size>0)
        {
          var data = docs.docs.map(function (documentSnapshot) { return documentSnapshot.data(); });
          res.send(data)
        }
        else //User Exists
        { res.send([]);  }
      });
    }else {res.send('invalid')}
  })
});
app.route('/seeds/getSexByID').get(
function(req, res) { 
  var ua=req.headers['user-agent'];
  auth.confirmAuthorization(req.headers['xmobile'],req.headers['authorization'],ua).then(doc=>{
    if(doc.length>0){
      app.locals.db.collection('xSex').where("name","==",req.query.xx).get().then(function(docs)
      {
        if(docs.size>0)
        {
          var data = docs.docs.map(function (documentSnapshot) { return documentSnapshot.data(); });
          res.send(data)
        }
        else //User Exists
        { res.send([]);  }
      });
    }else {res.send('invalid')}
  })
});
//////////////////////////////////////

};