'use strict';
var moment = require('moment');
var auth=require('./auth');

module.exports = function(app) {  
 app.route('/docs/createBlog').post(
    function(req, res) { 
    var ua=req.headers['user-agent'];
   auth.confirmAuthorization(req.body.mobile,req.headers['authorization'],ua).then(doc=>{
      if(doc.length>0){
        req.body['createdAt']=Date.now();req.body['xCreatedAt']=moment(Date.now()).format('DD MM YYYY, HH:mm:ss');
        app.locals.db.collection('xBlogs').add(req.body).then(result=>{
          console.log('addX: '+JSON.stringify(result.id))  // prints {id: "the unique id"}
          if(result.id!='')  {  res.send(JSON.stringify(result.id));}else{res.send("docError");}
        })
      }
      else {res.send('invalid')}
    })
});
app.route('/docs/createNewsFeed').post(
  function(req, res) { 
  var ua=req.headers['user-agent'];
  auth.confirmAuthorization(req.body.mobile,req.headers['authorization'],ua).then(doc=>{
    if(doc.length>0){
      req.body['createdAt']=Date.now();req.body['xCreatedAt']=moment(Date.now()).format('DD MM YYYY, HH:mm:ss');
      app.locals.db.collection('xFeeds').add(req.body).then(result=>{
        console.log('addX: '+JSON.stringify(result.id))  // prints {id: "the unique id"}
        if(result.id!='')  {res.send(JSON.stringify(result.id)); }else{res.send("docError");}
      })
    }
    else {res.send('invalid')}
  })
});

app.route('/docs/createMinutes').post(
  function(req, res) { 
  var ua=req.headers['user-agent'];
  auth.confirmAuthorization(req.body.mobile,req.headers['authorization'],ua).then(doc=>{
    if(doc.length>0){
      req.body['createdAt']=Date.now();req.body['xCreatedAt']=moment(Date.now()).format('DD MM YYYY, HH:mm:ss');
      app.locals.db.collection('xMinutes').add(req.body).then(result=>{
        console.log('addX: '+JSON.stringify(result.id))  // prints {id: "the unique id"}
        if(result.id!='')  {  res.send(JSON.stringify(result.id));}else{res.send("docError");}
      })
    }
    else {res.send('invalid')}
  })
});
app.route('/docs/createFinanceReport').post(
  function(req, res) { 
  var ua=req.headers['user-agent'];
  auth.confirmAuthorization(req.body.mobile,req.headers['authorization'],ua).then(doc=>{
    if(doc.length>0){
      req.body['createdAt']=Date.now();req.body['xCreatedAt']=moment(Date.now()).format('DD MM YYYY, HH:mm:ss');
      app.locals.db.collection('xFinanceReport').add(req.body).then(result=>{
        console.log('addX: '+JSON.stringify(result.id))  // prints {id: "the unique id"}
        if(result.id!='')  {  res.send(JSON.stringify(result.id));}else{res.send("docError");}
      })
    }
    else {res.send('invalid')}
  })
});

//////////////////////////////////////
// 

};