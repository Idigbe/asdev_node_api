'use strict';
var crypto = require('crypto');
var randomize = require('randomatic');var qrcode = require('yaqrcode');
var moment = require('moment');
var mailer=require('../helpers/mailer');
var auth=require('./auth');

module.exports = function(app) {  

  
  app.route('/users/createUser').post(
    function(req, res) { 
      try{
      // console.log('User Details: '+JSON.stringify(req.body));
      var ua=req.headers['user-agent'];
      auth.confirmAuthorization(req.headers['xmobile'],req.headers['authorization'],ua).then(doc=>{
        if(doc.length>0){
              var hashRes=saltHashPassword(req.body.xpin);
                        req.body['xpin']=hashRes.passwordHash;req.body['hasher']=hashRes.salt;
                        req.body['authPin']=hashRes.passwordHash;
                        req.body['createdAt']=Date.now();req.body['xCreatedAt']=moment(Date.now()).format('DD MM YYYY, HH:mm:ss');
                        getBaseUserRandomNumber().then(regCode=>{//console.log('regCode: '+regCode);
                              req.body['regCode']=regCode; 
                              app.locals.db.collection('xUsers').where('mobile', '==', req.body.mobile).get().then(function(existing_doc)
                              {
                                // console.log('existing_doc: '+JSON.stringify(existing_doc.size))
                                if(existing_doc.size==0)
                                {
                                  app.locals.db.collection('xUsers').add(req.body).then(doc => {
                                    // console.log('addX: '+JSON.stringify(doc.id))  // prints {id: "the unique id"}
                                    if(doc.id!='')  { 
                                      req.body['uid']=doc.id;
                                      app.locals.db.collection('xUsers').doc(doc.id).update(req.body).then(updatedoc => {
                                        // console.log('updateX: '+JSON.stringify(updatedoc))
                                        res.send(JSON.stringify(doc.id));
                                        // var msg=getRegistrationWelcomeBody(req.body.title+' '+req.body.fName+' '+req.body.lName,regCode);
                                        // if(msg!='')
                                        // {
                                        //   mailer.sendRegistrationWelcomeEmail('ASDEV Registration','no-reply@asdev.3nitix.guru',req.body.email,'ASDEV 81 CLUB Registration',msg).then(info=>{
                                        //     // res.send(JSON.stringify(doc.id));  // res.send(JSON.stringify(info))
                                        //   });
                                        // }
                                      })
                                    }
                                });
                                
                                }
                                else //User Exists
                                { res.send("User Exists!!");  }
                              });
                              
                            });
        }
        else {res.send('invalid')}
      })
          
    } catch(decode_err) {console.log("decode_err :"+decode_err);res.send('error')}         
  


});
app.route('/users/getAllUsers').get(
  function(req, res) { 
    var ua=req.headers['user-agent'];
    auth.confirmAuthorization(req.headers['xmobile'],req.headers['authorization'],ua).then(doc=>{
      if(doc.length>0){
        app.locals.db.collection('xUsers').get().then(function(docs)
        {
          if(docs.size>0)
          {
            var data = docs.docs.map(function (documentSnapshot) { return documentSnapshot.data(); });
            res.send(data)
          }
          else //User Exists
          { res.send([]);  }
        });
      }
      else {res.send('invalid')}
    })
  });
app.route('/users/getUsersByID').get(
  function(req, res) { 
    var ua=req.headers['user-agent'];
    auth.confirmAuthorization(req.headers['xmobile'],req.headers['authorization'],ua).then(doc=>{
      // console.log('Mobile: '+req.query.xmobile)
      ///////////////////////
      if(doc.length>0){
        app.locals.db.collection('xUsers').where("mobile","==",'+'+req.query.xmobile).get().then(function(docs)
        {
          if(docs.size>0)
          {
            var data = docs.docs.map(function (documentSnapshot) { return documentSnapshot.data(); });
            res.send(data)
          }
          else //User Exists
          { res.send([]);  }
        });
      }
      else {res.send('invalid')}
      ///////////////////////
    })
  });

function getRegistrationWelcomeBody(name,regcode){
  var msg='';
  msg += '<body>';            
  msg += '<table>';
  msg += '<tr>';
  msg += '<td><img src = "http://asdev.3nitix.guru/assets/img/logo.png" alt = "ASDEV" width = "120" height = "120" />';
  msg += '<hr />';
  msg += '</td>';
  msg += '</tr>';
  msg += '<tr>';
  msg += '<td><p>';
  msg += 'Dear <b> "'+name+'" </b>,<br />';
  msg += 'Welcome to the "ASDEV Membership" portal <br/>';
  msg += 'Please see below the required details';
  msg += '</p> <p>';
  msg += 'The following code <b> "'+regcode+'" </b> is your "Activation" and also your "Portal Reference Code"(PRC).<br/>';
  msg += 'Please use it to "Activate" your account before you proceed to payment!!';
  msg += '</p>';
  msg += 'Looking forward to relating with you!!<br/>';
  msg += 'Sincerely,<br/>';
  msg += 'ASDEV 81 CLUB';
  msg += '<hr/>';
  msg += '</td>';
  msg += '</tr>';
  msg += '<tr style="color:#fff; background-color:#120b27;">';
  msg += '<td>';
  msg += 'Copyright @ ASDEV 81 CLUB, ' + moment(Date.now()).format('YYYY');
  msg += '</td>';
  msg += '</tr>';
  msg += '</table>';
  msg += '</body>';
  return msg;
}
  var genRandomString = function(length){
        return crypto.randomBytes(Math.ceil(length/2))
                .toString('hex') /** convert to hexadecimal format */
                .slice(0,length);   /** return required number of characters */
    };
/**
 * hash password with sha512.
 * @function
 * @param {string} password - List of required fields.
 * @param {string} salt - Data to be validated.
 */
var sha512 = function(password, salt){
  var hash = crypto.createHmac('sha512', salt); /** Hashing algorithm sha512 */
  hash.update(password);
  var value = hash.digest('hex');
  return {
      salt:salt,
      passwordHash:value
  };
};
function saltHashPassword(userpassword) {
  var salt = genRandomString(16); /** Gives us salt of length 16 */
  var passwordData = sha512(userpassword, salt);
  // console.log('UserPassword = '+userpassword); console.log('Passwordhash = '+passwordData.passwordHash);console.log('nSalt = '+passwordData.salt);
  return passwordData;
}


function getBaseUserRandomNumber()
{ 
  return new Promise((resolve,reject)=>
  {
    var rd= randomize('A0', 6);var cnt=0;
    app.locals.db.collection('xUsers').where('regCode','==',rd).get().then(function(docs)
    {
      if(docs.size>0)//Code Exists
      {getBaseUserRandomNumber(); }
      else //Return Code
      { resolve(rd) ;}
    });
  }
  )
}

function getLastFourDigits(number){//2348098367528
  var num='';
  num=number.replace("+","");num=num.substring(9)
  return num;
}



};